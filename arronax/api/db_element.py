from py2neo import Graph, Node, NodeMatcher, Relationship
from .logger_lib import logger


def class_from_name(class_name):
    """Returns class object by his name

    :param class_name:
    :return:
    """
    module = __import__("api.{}".format(class_name))
    _class = getattr(module, class_name.capitalize())
    return _class


class DB_element(object):
    """Wrapper py2neo and arronax API. Contains all functions that make the link between arronax.api objects and db

        :param graph:
        :param db_type:
        :param name:
        :param db_object:
        :param parent:
        """

    def __init__(self, name=None, db_type=None, db_object=None, parent=None, link_name="parent", filter=None):
        if not (isinstance(name, str) or isinstance(name, unicode)):
            raise ValueError("Initialise DB_element with name not str is forbidden! (name: {})".format(name))
        self.fullname = name
        if parent is not None:
            self.fullname = "{}_{}".format(parent.fullname, name)
        self.connexion = Connexion()
        graph = None  # removed from init param?
        if graph is None:
            graph = self.connexion.graph
        self.graph = graph

        self.db_object, self.__db_objects = (db_object, list())
        self.db_type = db_type
        self.name = name
        self.parent = parent
        self.link_name = link_name
        self.filter = filter

    def create(self, properties=None):
        """Create self arronax.api object in the database. Abort if object already exists.

        :param properties: (dict) additionnal and optionnal dictionnary to add on the db item on creation
        :return: (arronax.api.<object>) created item (type is arronax.api.Project ect...) depending of self.db_type
        """
        if properties is None:
            properties = {}
        if self.find_one():  # item already exists
            logger.warning("Item <{}> {} already exists in the database!".format(self.db_type, self.name))
            return self
        self.db_object = Node(self.db_type, name=self.name, fullname=self.fullname, **properties)
        self.graph.create(self.db_object)

        if self.parent is not None:  # TODO
            relationship = Relationship(self.parent.db_object, "parent", self.db_object)
            self.graph.create(relationship)
        logger.debug("Created <{}> {} successfully in the database !".format(self.db_type, self.name))
        return self

    def find(self, unique=False, filter=None):
        """Find items the db --> feed the private self.__db_objects

        :param unique: (bool) default is False, when True, query doesn't parse regex (search for exact name)
        :return: (list of dict) results of database objects found. [] if nothing match
        """
        if self.parent is None:
            # If parent is None, search neo4j nodes directly by their names
            self.__db_objects = list(
                self.connexion.node_matcher.match(self.db_type).where("_.name =~ '{}'".format(self.name))
            )
        else:
            # Check that parent name and relationship match
            args = {
                "parent_db_type": self.parent.db_type,
                "parent_name": '{name:"%s"}' % self.parent.name,
                "link_name": self.link_name,
                "db_type": self.db_type,
                "unique_char": "~",
                "name": self.name,
                "fullname": self.fullname,
            }
            if unique:
                args["unique_char"] = ""

            cmd = """match (a:{parent_db_type} {parent_name})-[r:{link_name}]->(b:{db_type}) where b.name ={unique_char} "{name}" AND b.fullname ={unique_char} "{fullname}" """.format(
                **args
            )

            if filter is not None and isinstance(filter, dict):
                for key, value in filter.items():
                    cmd += 'AND b.{} ={} "{}" '.format(key, args["unique_char"], value)

            cmd += " return b"
            records = list(self.graph.run(cmd))
            self.__db_objects = [dict(record)["b"] for record in records]

        return self.__db_objects

    def find_one(self):
        """Same as find() function but returns a single item
        /!\ WARNING /!\ Returns the first item if more than one item match (meaning items on same level or same
        parent-children configuration have the exact same name) !

        :return: (dict) result of database object found. None if nothing match.
        """
        exists = self.find(unique=True)
        if exists is None or not len(exists):
            return None
        self.db_object = self.__db_objects[0]
        return self.db_object

    def remove(self):
        """Remove item in the db.
        TODO Not yet implemented

        :return:
        """
        if self.db_object is None:
            raise ValueError("There is no database object attached to the current arronax object to remove!")
        raise NotImplementedError()

    def update_properties(self, properties):
        """Update properties in the database of current item

        :param properties: (dict) dictionary of key/values to update
        """
        logger.debug("Update <{}> properties:{}".format(self.name, properties))
        self.db_object.update(properties)
        self.connexion.graph.push(self.db_object)

    @property
    def ax_object(self):
        """Feed current DB_element (instance or overclass) db_object
        Return typed arronax.api.<class> object of current item

        :return: (arronax.api.<class>) equivalent class object of current db_object
        """
        exists = self.find_one()
        if exists is None:
            return None
        return class_from_name(self.db_type)(name=self.db_object["name"], parent=self.parent, db_object=self.db_object)

    @property
    def ax_objects(self):
        """Feed current DB_element (instance or overclass) __db_objects
        Return typed arronax.api.<class> objects of current item

        :return: (list of arronax.api.<class>) equivalent class objects of current db_object
        """
        exists = self.find(filter=self.filter)
        if exists is not None:
            return sorted(
                [
                    class_from_name(self.db_type)(name=db_object["name"], parent=self.parent, db_object=db_object)
                    for db_object in self.__db_objects
                ],
                key=lambda x: x.name,
            )
        return []

    @property
    def properties(self):
        return dict(self.db_object)

    def parent_to(self, parent, link_name="parent"):
        """Parent self item to the arronax.api object (arronax.User to arronax.Project for exemple)

        :param parent: (arronax.api.<class>) of parent to link
        :param link_name: (str) name of the parent link (ex: parent / assigned / linked ect...)
        :return: (py2neo.RelationShip) the relationship between objects
        """
        relationship = Relationship(parent.db_object, link_name, self.db_object)
        self.graph.create(relationship)
        return relationship


class Connexion(object):
    """Establish connexion to the database

    """

    def __init__(self, host, user, password):  # TODO protect credentials
        self.host = host
        self.user = user
        self.password = password
        self.__graph__ = Graph(host=self.host, user=self.user, password=self.password)  # 7474, 7687( default)

    @property
    def graph(self):
        return self.__graph__

    @property
    def node_matcher(self):
        return NodeMatcher(self.graph)
