from .db_element import DB_element
import fileseq
from .logger_lib import logger


class FileCore(object):
    """Create and get File

    """

    def create_file(self, file_path, properties=None, already_exists=True):
        """Create File in the db. Checks if already exists with this name. Returns existing one if already created
        You can use a direct framed path or with padding # or %04d
        Exemple:
            .../PBA1180_104_MP01_V001.#.exr
            .../PBA1180_104_MP01_V001.1000.exr
            .../PBA1180_104_MP01_V001.%04d.exr
        The module fileseq will automatically found the start/end frames, padding ect...

        :param file_path: (str) file path of object to create.
        :param properties: (dict) if specified, override automatic properties (based on file)
        :param already_exists: (bool) if True, file supposed to exists on disk. Will crash if you're a lier. When True
            properties will be automatically created from fileseq module. Else you have to specify mannually
        :return: (arronax.api.File) object or None of created object
        """
        if properties is None:
            properties = {}
        #  _properties is the local var that is build from :param properties
        _properties = dict(frame_range="", start=0, end=0, file_path=file_path, extension="", zfill=4,)
        name = file_path

        if already_exists:
            seq = fileseq.findSequenceOnDisk(file_path)
            seq.setFramePadding("#")

            name = seq.format(template="{basename}{padding}{extension}")  # no padding in name
            _properties["frame_range"] = seq.frameRange()
            _properties["start"] = seq.start()
            _properties["end"] = seq.end()
            _properties["file_path"] = seq.format(template="{dirname}{basename}{padding}{extension}")
            _properties["extension"] = seq.extension()
            _properties["zfill"] = seq.zfill()

        for key, value in properties.items():
            _properties[key] = value
            if key not in _properties.keys():
                logger.warning(
                    "Key '{}' is not accepted for file. Currently supported are: {}".format(
                        key, str(_properties.keys())
                    )
                )
                continue
            _properties[key] = value

        file = File(name=name, parent=self)
        return file.create(properties=_properties)

    def get_file(self, file_name):
        """Get unique file item under current object. Returns None if nothing found

        :param file_name: (str) name to find with
        :return: (arronax.api.File) object or None of founded item
        """
        file = File(name=file_name, parent=self)
        return file.ax_object

    def get_files(self, file_pattern=".*"):
        """Get multiple file items under current object. Returns [] if nothing found

        :param file_pattern: (str) name to find with
        :return: (list of arronax.api.File) objects or [] of founded items
        """
        file = File(name=file_pattern, parent=self)
        return file.ax_objects


class File(DB_element):
    def __init__(self, name, parent=None, db_object=None):
        super(File, self).__init__(name=name, db_type="file", parent=parent, db_object=db_object)

    def __getattr__(self, item):
        return self.db_object[item]

    def link_to_file(self, file):
        return self.parent_to(file, link_name="linked")
