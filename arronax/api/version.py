from .db_element import DB_element
from .file import FileCore

# from .task import TaskCore


class VersionCore(object):
    """Create and get Version

    """

    def create_version(self, version_num, file=""):
        """Create Version in the db. Checks if already exists with this name. Returns existing one if already created

        :param version_num: (int) version number
        :param file: (str)
        :return: (arronax.api.File) object or None of created object
        """
        version_name = "v{:03d}".format(version_num)
        properties = {"version_num": version_num, "file": file}

        version = Version(name=version_name, parent=self)
        return version.create(properties=properties)

    def get_version(self, version_name):
        """Get unique version item under current object. Returns None if nothing found

        :param version_name: (str) name to find with
        :return: (arronax.api.Version) object or None of founded item
        """
        version = Version(name=version_name, parent=self)
        return version.ax_object

    def get_versions(self, version_pattern=".*"):
        """Get multiple version items under current object. Returns [] if nothing found

        :param version_pattern: (str) name to find with
        :return: (list of arronax.api.Version) objects or [] of founded items
        """
        version = Version(name=version_pattern, parent=self)
        return version.ax_objects


class Version(DB_element, FileCore):
    def __init__(self, name, parent=None, db_object=None):
        super(Version, self).__init__(name=name, db_type="version", parent=parent, db_object=db_object)

    def __getattr__(self, item):
        return self.db_object[item]

    def link_to_version(self, version):
        """Link current version to an other. Informations can be added on the link such as overrides.

        :param version: (arronax.api.Version) version to link to
        :return: (py2neo.RelationShip) the relationship between objects
        """
        return self.parent_to(version, link_name="linked")
