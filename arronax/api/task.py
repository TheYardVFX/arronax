from .db_element import DB_element
from .version import VersionCore
from .asset import AssetCore


class TaskCore(object):
    """Create and get Task

    """

    def create_task(self, task_name):
        """Create Task in the db. Checks if already exists with this name. Returns existing one if already created

        :param task_name: (str) name of task te create
        :return: (arronax.api.Task) object or None of created object
        """
        task = Task(name=task_name, parent=self)
        return task.create()

    def get_task(self, task_name):
        """Get unique task item under current object. Returns None if nothing found

        :param task_name: (str) name to find with
        :return: (arronax.api.Task) object or None of founded item
        """
        task = Task(name=task_name, parent=self)
        return task.ax_object

    def get_tasks(self, task_pattern=".*"):
        """Get multiple task items under current object. Returns [] if nothing found

        :param task_pattern: (str) name to find with
        :return: (list of arronax.api.Task) objects or [] of founded items
        """
        task = Task(name=task_pattern, parent=self)
        return task.ax_objects


class Task(DB_element, VersionCore, AssetCore):  # AssetCore
    def __init__(self, name, parent=None, db_object=None):
        super(Task, self).__init__(name=name, db_type="task", parent=parent, db_object=db_object)
