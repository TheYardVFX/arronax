from .db_element import DB_element
from .version import VersionCore


class AssetCore(object):
    """Create and get Asset

    """

    def create_asset(self, asset_name, properties={}):
        """Create Asset in the db. Checks if already exists with this name. Returns existing one if already created

        :param asset_name: (str) name of asset te create
        :return: (arronax.api.Asset) object or None of created object
        """
        asset = Asset(name=asset_name, parent=self)
        return asset.create(properties)

    def get_asset(self, asset_name):
        """Get unique asset item under current object. Returns None if nothing found

        :param asset_name: (str) name to find with
        :return: (arronax.api.Asset) object or None of founded item
        """
        asset = Asset(name=asset_name, parent=self)
        return asset.ax_object

    def get_assets(self, asset_pattern=".*", filter=None):
        """Get multiple asset items under current object. Returns [] if nothing found

        :param asset_pattern: (str) name to find with
        :return: (list of arronax.api.Asset) objects or [] of founded items
        """
        asset = Asset(name=asset_pattern, parent=self)
        asset.filter = filter
        return asset.ax_objects


class Asset(DB_element, VersionCore):
    def __init__(self, name, parent=None, db_object=None):
        super(Asset, self).__init__(name=name, db_type="asset", parent=parent, db_object=db_object)
