import os
from .asset import Asset
from .logger_lib import logger
from .db_element import Connexion
from .project import Project, get_project, create_project
from .task import Task
from .user import User, get_users, get_user, create_user
from .shot import Shot
from .version import Version
from .file import File

__connexion__ = None


def connect():
    global __connexion__
    if __connexion__ is None:
        logger.info("Connect to the neo4j database...")
        db_user = os.environ["ARONNAX_DB_USER"]
        db_password = os.environ["ARONNAX_DB_PASSWORD"]
        db_host = os.environ["ARONNAX_DB_HOST"]
        __connexion__ = Connexion(db_host, db_user, db_password)
    else:
        # raise RuntimeError("Database name has already been set.")
        logger.debug("Database connexion has already been made.")
