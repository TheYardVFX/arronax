import logging
import logstash

logger = None


def get_logger():
    # create a logger

    logger = logging.getLogger("Arronax")
    for handler in logger.handlers:
        logger.removeHandler(handler)

    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)
    logger.setLevel(logging.INFO)
    return logger


logger = get_logger()
