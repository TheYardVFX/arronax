from .db_element import DB_element


class UserCore(object):
    """Create and get User

    """

    def create_user(self, user_name):
        """Create User in the db. Checks if already exists with this name. Returns existing one if already created

        :param user_name: (str) name of user te create
        :return: (arronax.api.User) object or None of created object
        """
        user = User(name=user_name, parent=self)
        return user.create()

    def get_user(self, user_name):
        """Get unique user item under current object. Returns None if nothing found

        :param user_name: (str) name to find with
        :return: (arronax.api.User) object or None of founded item
        """
        user = User(name=user_name, parent=self)
        return user.ax_object

    def get_users(self, user_pattern=".*"):
        """Get multiple user items under current object. Returns [] if nothing found

        :param user_pattern: (str) name to find with
        :return: (list of arronax.api.User) objects or [] of founded items
        """
        user = User(name=user_pattern, parent=self)
        return user.ax_objects


class User(DB_element):
    def __init__(self, name, parent=None, db_object=None):
        super(User, self).__init__(name=name, db_type="user", parent=parent, db_object=db_object, link_name="assigned")

    def __db_object_project(self, name):
        db_object = DB_element(name=".*", db_type="project", parent=self, link_name="assigned")
        return db_object

    def get_projects(self):
        """Find all projects assigned to the user

        :return: (list of arronax.api.Project) objects or [] of founded items
        """
        db_object = self.__db_object_project(".*")
        return db_object.ax_objects

    def assign_project(self, project):
        """Assign self user to the parsed project

        :param project: (arronax.api.Project) project to assign to
        :return: (py2neo.RelationShip) the relationship between objects
        """
        return self.parent_to(project, "assigned")


def create_user(user_name):
    # TODO use UserCore function, but its a metaclass so that needs to be instanced as DB_element before ?!
    user = User(name=user_name, parent=None)
    result = user.create()
    return result


def get_user(user_name):  # TODO +1
    user = User(name=user_name, parent=None)
    return user.ax_object


def get_users(user_pattern=".*"):  # TODO +1
    user = User(name=user_pattern, parent=None)
    return user.ax_objects
