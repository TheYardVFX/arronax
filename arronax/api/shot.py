from .db_element import DB_element
from .asset import AssetCore
from .task import TaskCore


class ShotCore(object):
    """Create and get Shot

    """

    def create_shot(self, shot_name):
        """Create Shot in the db. Checks if already exists with this name. Returns existing one if already created

        :param shot_name: (str) name of shot te create
        :return: (arronax.api.Shot) object or None of created object
        """
        shot = Shot(name=shot_name, parent=self)
        return shot.create()

    def get_shot(self, shot_name):
        """Get unique shot item under current object. Returns None if nothing found

        :param shot_name: (str) name to find with
        :return: (arronax.api.Shot) object or None of founded item
        """
        shot = Shot(name=shot_name, parent=self)
        return shot.ax_object

    def get_shots(self, shot_pattern=".*"):
        """Get multiple shot items under current object. Returns [] if nothing found

        :param shot_pattern: (str) name to find with
        :return: (list of arronax.api.Shot) objects or [] of founded items
        """
        shot = Shot(name=shot_pattern, parent=self)
        return shot.ax_objects


class Shot(DB_element, AssetCore, TaskCore):
    def __init__(self, name, parent=None, db_object=None):
        super(Shot, self).__init__(name=name, db_type="shot", parent=parent, db_object=db_object)
