from .db_element import DB_element
from .shot import ShotCore
from .task import TaskCore
from .user import UserCore


class ProjectCore(object):
    """Create and get Project

    """

    def create_project(self, project_name):
        """Create Project in the db. Checks if already exists with this name. Returns existing one if already created

        :param project_name: (str) name of project te create
        :return: (arronax.api.Project) object or None of created object
        """
        project = Project(name=project_name, parent=self)
        return project.create()

    def get_project(self, project_name):
        """Get unique project item under current object. Returns None if nothing found

        :param project_name: (str) name to find with
        :return: (arronax.api.Project) object or None of founded item
        """
        project = Project(name=project_name, parent=self)
        return project.ax_object

    def get_projects(self, project_pattern=".*"):
        """Get multiple projects items under current object. Returns [] if nothing found

        :param project_pattern: (str) name to find with
        :return: (list of arronax.api.Project) objects or [] of founded items
        """
        project = Project(name=project_pattern, parent=self)
        return project.ax_objects


class Project(DB_element, ShotCore, TaskCore, UserCore):
    def __init__(self, name, parent=None, db_object=None):
        super(Project, self).__init__(name=name, db_type="project", parent=parent, db_object=db_object)

    def assign_user(self, user):
        pass

    def get_assets(self, asset_name_pattern=""):
        pass


def get_project(project_name):
    # TODO use ProjectCore function, but its a metaclass so that needs to be instanced as DB_element before ?!
    project = Project(name=project_name)
    return project.ax_object


def create_project(project_name):  # TODO +1
    project = Project(name=project_name)
    project.create()
    return project
