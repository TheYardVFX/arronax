# neo-arronax

## Decription

This api allow you to manupulate arronax.api.***objects*** instead of query/create directly into neo4j database.

### Install

```
# Install dependencies (you should do it in a virtual-env)
pip install -r requirements.txt
```

### How to use it

This *package* is supposed to be in ***PYTHONPATH*** and all requirements are satisfied.\
Works in **python3.6** and **python2.7**

```python
import arronax.api as ax
ax.connect()

# Quick exemple usage to get project and user, and assign him to it.
project_m2 = ax.get_project("minuscule_2")
vincent = ax.get_user("vincent")
vincent.assign_project(project_m2)
```

### Authors

**Vincent De Bellis** [vincent.debellis@gmail.com](vincent.debellis@gmail.com)

### License

Copyright © 2020, [The Yard VFX](https://theyard-vfx.com/).
